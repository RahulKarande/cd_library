package com.example.networkretro.ApiClient

import com.example.networkretro.Utils.Constant
import com.example.networkretro.Utils.Constant.baseUrl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object UserClient{



        var retrofit: Retrofit? = null
        val client: Retrofit
            get() {
                if (retrofit == null) {
                    retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                }
                return retrofit!!
            }

}