package com.example.networkretro.Network

import com.example.networkretro.ApiClient.UserClient
import com.example.networkretro.Interface.ApiInterface
import com.example.networkretro.Utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetworkAPI {


    fun getUserDetails(serviceUrl: String): String {
        Constant.baseUrl = serviceUrl
        var serviceResponse: String = ""
        var apiServices = UserClient.client.create(ApiInterface::class.java)
        apiServices.userDetails().enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                serviceResponse = call.toString()
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                serviceResponse = response.toString()
            }

        })
        return serviceResponse
    }
}