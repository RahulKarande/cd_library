package com.example.networkretro.Interface


import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface
{
    @GET("users/?per_page=12&amp;page=1")
    fun userDetails(): Call<String>
}